const { GraphQLScalarType, Kind } = require('graphql');
const { UserInputError } = require('apollo-server');

const moment = require('moment');

const parseWeekDay = (date) => {
    const parsedDate = moment(date, 'MM/DD/YYYY');
    if (!parsedDate.isValid()) {
      throw new UserInputError('value is not a valid date');
    }

    if (parsedDate.day() >= 6) {
			throw new UserInputError('Provided value is not a week day');
    }
		return parsedDate.format('MM/DD/YYYY');
};

const weekDateScalar = new GraphQLScalarType({
  name: 'WeekDate',
  description: 'Week Date (between Mon-Fri) custom scalar type',
  serialize(value) {
    return parseWeekDay(value); // Convert outgoing Date to string for JSON
  },
  parseValue(value) {
    return parseWeekDay(value); // Convert incoming integer to Date
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.STRING) {
      return parseWeekDay(ast.value); // Convert hard-coded AST string to Date
    }
    return null; // Invalid hard-coded value (not a string)
  },
});

module.exports = {
  weekDateScalar,
};
