const { gql } = require('apollo-server');


// The GraphQL schema
module.exports = gql`
	"location data"
  type Location {
		id: ID!,
		area: String!,
		department: Department
	}
`;