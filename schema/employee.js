const { gql } = require('apollo-server');



// The GraphQL schema
module.exports = gql`
	"employee data"
  type Employee {
		id: ID!,
		name: String!,
		department: Department,
		"Joining date of the employee. Format(MM/DD/YYYY)" 
		joiningDate: WeekDate! # Joining date of the employee 
	}
`;
