const { gql } = require('apollo-server');
const employeeSchema = require('./employee');
const Department = require('./department');
const location = require('./location');
const { Search } = require('./search');




// The GraphQL schema
const typeDefs = gql`
  scalar WeekDate

  type Query {
    employees: [Employee!]!
    employee(id: ID!): Employee
    department(id: ID!): Department
    departments: [Department!]!
		search(text: String!): [Search!]!
  }

  ${employeeSchema}
  ${Department}
  ${location}
	${Search}
`;

module.exports = {
  typeDefs,
};
