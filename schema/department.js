const { gql } = require('apollo-server');

// The GraphQL schema
module.exports = gql`
	"department data"
  type Department {
		id: ID!,
		name: String!,
		type: String!,
		# location: Location
	}
`;