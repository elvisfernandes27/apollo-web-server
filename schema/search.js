const { gql } = require('apollo-server');



// The GraphQL schema
const Search = gql`
	"search data"
  union Search = Employee | Department
`;

module.exports = {
	Search
}
