# Apollo GraphQL Server

Basic graphql web server  

## Prerequisites

* `npm i` -- to install packages  

## Steps

* `npm run json-server`
    will start a dummy json server with data in data.json  

* `npm run dev-server`
    will start the apollo server  
