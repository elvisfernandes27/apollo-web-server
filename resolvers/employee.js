const axios = require('axios')

exports.employees = async () => {
	const res = await axios.get('http://localhost:3000/employees')
	return res.data;
};

exports.employee = async (_, { id }) => {
	const res = await axios.get(`http://localhost:3000/employees/${id}`)
	return res.data;
}

exports.Employee = {
	department: async ({department = ''}) => {
		if (!department || department === '') {
			return []
		}

		const res = await axios.get(`http://localhost:3000/departments/${department}`)
		return res.data;
	}
}
