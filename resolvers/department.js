const axios = require('axios')

exports.departments = async () => {
	const res = await axios.get('http://localhost:3000/departments')
	return res.data;
};

exports.department = async (_, { id }) => {
	const res = await axios.get(`http://localhost:3000/departments/${id}`)
	return res.data;
}

// exports.Department = {
// 	location: async (_, {locationId = ''}) => {
// 		if (!locationId || locationId === '') {
// 			return []
// 		}

// 		const res = await axios.get(`http://localhost:3000/locations/${locationId}`)
// 		return res.data;
// 	}
// }