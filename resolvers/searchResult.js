const SearchResult = {
  __resolveType(obj) {
    console.log('logger -  ~ file: searchResult.js ~ line 3 ~ __resolveType ~ obj', obj)
    // Only Employee has a department field
    if ('department' in obj) {
      return 'Employee';
    }

		return 'Department';
  },
};

module.exports = {
  SearchResult,
};
