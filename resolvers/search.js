const axios = require('axios');

exports.search = async () => {
	const res = await Promise.allSettled([
		axios.get('http://localhost:3000/employees'),
		axios.get('http://localhost:3000/departments')
	])
	return res.filter(({status}) => status === 'fulfilled')
	.reduce((result, {value}) => result.concat(value.data), [])
  // const res = await axios.get('http://localhost:3000/employees');
  // return res.data;
};

