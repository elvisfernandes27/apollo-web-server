// A map of functions which return data for the schema.
const { employees, employee, Employee } = require('./employee');
const { department, departments } = require('./department');
const { weekDateScalar } = require('../definitions/weekDate');
const { SearchResult } = require('./searchResult');
const { search } = require('./search');




exports.resolvers = {
	Search: SearchResult,
  WeekDate: weekDateScalar,
  Query: {
    employees,
    employee,
    department,
    departments,
		search
  },
  Employee,
};
